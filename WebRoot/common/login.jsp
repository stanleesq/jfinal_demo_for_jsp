<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link href="${ctx }/css/manage.css" media="screen" rel="stylesheet" type="text/css" />
<script src="${ctx }/js/jquery-1.4.4.min.js" type="text/javascript"></script>
</head>
<body>
	<div class="manage_container">
		<%@ include file="/common/top.jsp"%>
		<form action="${ctx }/common/login" method="post">
			<div class="main">
				<h1>JFinal Demo 项目首页 登陆</h1>
				<div align="center">
					<br />
					<table style="width: 300px" align="center" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 200px;height: 30px">用户名：<input type="text" name="loginUserName" size="20" /></td>
							<td><font color="red">${loginUserNameMsg }</font></td>
						</tr>
						<tr>
							<td style="width: 200px;height: 30px">密&nbsp;&nbsp;&nbsp;码：<input type="password" name="loginPassword" size="20" /></td>
							<td><font color="red">${loginPasswordMsg }</font></td>
						</tr>
						<tr>
							<td><font color="red">${loginError }</font></td>
						</tr>
						<tr>
							<td align="center">
								<input type="submit" value="登陆" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重置" />
							</td>
						</tr>
					</table>
					<br />
				</div>
			</div>
		</form>
	</div>
</body>
</html>
