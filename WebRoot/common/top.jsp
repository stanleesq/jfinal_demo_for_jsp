<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ page import="com.demo.user.Resources"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link href="${ctx }/css/manage.css" media="screen" rel="stylesheet"
	type="text/css" />
<script src="${ctx }/js/jquery-1.4.4.min.js" type="text/javascript"></script>
</head>
<body>
	<div class="manage_head">
		<div class="manage_logo">
			<a href="http://code.google.com/p/jfinal" target="_blank">JFinal
				web framework</a>
		</div>
		<div id="nav">
			<ul>
				<li><a href="${ctx }/"><b>首页</b></a></li>
				<li><a href="${ctx }/blog"><b>Blog管理</b></a></li>
				<%
					if (Resources.userActionKeyList.contains("/resources")) {
				%>
				<li><a href="${ctx }/resources"><b>资源管理</b></a></li>
				<%
					}
				%>
				<%
					if (session.getAttribute("loginUser") != null) {
				%>
				<%
					if (Resources.userActionKeyList.contains("/role")) {
				%>
				<li><a href="${ctx }/role"><b>角色管理</b></a></li>
				<%
					}
				%>
				<%
					if (Resources.userActionKeyList.contains("/user")) {
				%>
				<li><a href="${ctx }/user"><b>用户管理</b></a></li>
				<%
					}
				%>
				<li><a href="${ctx }/common/updatePassword"><b>修改密码</b></a></li>
				<li><a href="${ctx }/common/loginOut"><b>退出</b></a></li>
				<%
					} else {
				%>
				<li><a href="${ctx }/common/toLogin"><b>登陆</b></a></li>
				<%
					}
				%>
			</ul>
		</div>
	</div>
</body>
</html>
