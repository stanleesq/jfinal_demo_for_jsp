<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link href="${ctx }/css/manage.css" media="screen" rel="stylesheet" type="text/css" />
<script src="${ctx }/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		$("#subButton").click(function() {

			var oldPassword = $("#oldPassword").val();
			var newpassword = $("#newPassword").val();
			var confirmpassword = $("#confirmPassword").val();
			
			if (oldPassword.length < 3) {
				alert("原密码格式不对，长度至少为3");
				return false;
			}
			
			if (newpassword.length < 3) {
				alert("新密码格式不对，长度至少为3");
				return false;
			}
			
			if (newpassword != confirmpassword) {
				alert("两次输入的密码不一致，请重新输入");
				return false;
			}

		});
	});
</script>
</head>
<body>
	<div class="manage_container">
		<%@ include file="/common/top.jsp"%>
		<div class="main">
			<h1>个人中心 ---&gt; 修改密码</h1>
			<div class="form_box">
				<form action="${ctx }/common/savePassword" method="post">
					<fieldset class="solid">
						<label>密码只能为字母+数字组合,长度为3-12</label>
						<br />
						<div>
							<label>原密码</label> <input type="password" name="oldPassword" id="oldPassword" value=""  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" maxlength="12"/>
							<font color="red">${passwordMsg}</font>
						</div>
						<div>
							<label>新密码</label> <input type="password" name="newPassword" id="newPassword"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" value="" maxlength="12"/>
						</div>
						<div>
							<label>确认密码</label> <input type="password" name="confirmPassword" id="confirmPassword"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" value="" maxlength="12"/>
						</div>
						<div>
							<label>&nbsp;</label> <input value="提交" type="submit" id="subButton" />
							<input type="button" value="返回" onclick="javascript:history.go(-1);" />
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</body>
</html>

