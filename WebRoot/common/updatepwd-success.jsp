<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link href="/css/manage.css" media="screen" rel="stylesheet"
	type="text/css" />
<script src="/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script type="text/javascript">
	var t=3;
	$(function(){
		setInterval("count()", 1000);
		$("#timeCount").html(t);
	});
	
	function count(){
		 t --;  //秒数自减
		 if(t >= 0)
		 	$("#timeCount").html(t);  //刷新当前的秒数，重新显示秒数
		 else{
		    clearInterval();//这个可以不用，因为页面都要跳转了，要了也没多大差别
		    window.location.href="${ctx}/";  // 设置跳转的链接
		 }
	}
</script>
</head>
<body>
	<%
		response.setHeader("refresh","3;URL=/");
	 %>
	<div class="manage_container">
		<%@ include file="/common/top.jsp"%>
		<div class="main">
			<h1>个人中心 ---&gt; 修改密码</h1>
			<div class="form_box">
				<font>密码修改成功！</font>
				<br />
				<span id="timeCount" style="color: red"></span>秒后自动跳转到首页！
				<br />
				如果没有跳转请点击<a href="/">这里</a>
			</div>
			<br />
			<br />
		</div>
	</div>
</body>
</html>

