<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link href="${ctx }/css/manage.css" media="screen" rel="stylesheet"
	type="text/css" />
<script src="${ctx }/js/jquery-1.4.4.min.js" type="text/javascript"></script>
</head>
<body>
	<div class="manage_container">
		<%@ include file="/common/top.jsp"%>
		<div class="main">
			<h1>资源管理 ---&gt; <c:if test="${resources.id != null }">修改</c:if><c:if test="${resources.id == null }">创建</c:if>资源</h1>
			<div class="form_box">
				<form action="${ctx }/resources/save" method="post">
					<fieldset class="solid">
						<input type="hidden" name="resources.id" value="${resources.id}" />
						<div>
							<label>action_key</label>
							<input type="text" name="resources.action_key" value="${resources.action_key}" />${contentMsg}
						</div>
						<div>
							<label>controller_key</label>
							<input type="text" name="resources.controller_key" value="${resources.controller_key}" />${titleMsg}
						</div>
						<div>
							<label>描述</label>
							<input type="text" name="resources.descn" value="${resources.descn }"/>${contentMsg}
						</div>
						<div>
							<label>&nbsp;</label> <input value="提交" type="submit" /><input type="button" value="返回" onclick="javascript:history.go(-1);"/>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</body>
</html>

