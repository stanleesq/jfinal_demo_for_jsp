<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link href="${ctx }/css/manage.css" media="screen" rel="stylesheet"
	type="text/css" />
<script src="${ctx }/js/jquery-1.4.4.min.js" type="text/javascript"></script>
</head>
<body>
	<div class="manage_container">
		<%@ include file="/common/top.jsp"%>
		<div class="main">
			<h1>资源管理 ---&gt; <c:if test="${role.id != null }">修改</c:if><c:if test="${role.id == null }">创建</c:if>角色</h1>
			<div class="form_box">
				<form action="${ctx }/role/save" method="post">
					<fieldset class="solid">
						<input type="hidden" name="role.id" value="${role.id}" />
						<div>
							<label>名称</label>
							<input type="text" name="role.name" value="${role.name}" />
						</div>
						<div>
							<label>描述</label>
							<input type="text" name="role.descn" value="${role.descn }"/>
						</div>
						<div>
							<label>资源</label>
							<%
								List<Resources> allResList = (List) request.getAttribute("allResList");
								List<Integer> checkedResList = (List) request.getAttribute("checkedResIdList");
								for(int i = 0; i < allResList.size(); i++){
									Map obj = (Map)allResList.get(i);
									int resId = Integer.parseInt(obj.get("id").toString());
									if(checkedResList != null && checkedResList.contains(resId)){
									%>
										<input type="checkbox" name="resourcesId" value="<%=resId%>" checked="checked"/><%=obj.get("action_key")%>
									<%
									}else{
									%>
										<label><input type="checkbox" name="resourcesId" value="<%=resId%>"/><%=obj.get("action_key")%></label>
									<%
									}
								}
							%>
						</div>
						<div>
							<label>&nbsp;</label> <input value="提交" type="submit" /><input type="button" value="返回" onclick="javascript:history.go(-1);"/>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</body>
</html>

