<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link href="${ctx }/css/manage.css" media="screen" rel="stylesheet"
	type="text/css" />
<script src="${ctx }/js/jquery-1.4.4.min.js" type="text/javascript"></script>
</head>
<body>
	<div class="manage_container">
		<%@ include file="/common/top.jsp"%>
		<div class="main">
			<h1>
				用户管理&nbsp;&nbsp; <a href="/user/edit">创建用户</a>
			</h1>
			<div class="table_box">
				<table class="list">
					<tbody>
						<tr>
							<th width="4%">id</th>
							<th width="20%">用户名</th>
							<th width="20%">邮箱</th>
							<th width="20%">描述</th>
							<th width="20%">操作</th>
						</tr>
						<c:forEach items="${userList}" var="user">
							<tr>
								<td style="text-align: left;"><c:out value="${user.id}" default="" /></td>
								<td style="text-align: left;">${user.name}</td>
								<td style="text-align: left;">${user.email}</td>
								<td style="text-align: left;">${user.descn}</td>
								<td style="text-align: left;">&nbsp;&nbsp;<a href="${ctx }/userdelete/${user.id}">删除</a> &nbsp;&nbsp;<a href="${ctx }/user/edit/${user.id}">修改</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>