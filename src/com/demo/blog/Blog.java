package com.demo.blog;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

/**
 * Blog model.
 */
@SuppressWarnings("serial")
public class Blog extends Model<Blog> {
	public static final Blog dao = new Blog();

	public static List<Blog> getAllBlog() {
		return dao.find("select * from jf_demo_blog order by id asc");
	}
}