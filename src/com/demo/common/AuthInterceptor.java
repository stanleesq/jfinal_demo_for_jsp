package com.demo.common;

import com.demo.user.Resources;
import com.demo.user.User;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;

/**
 * Copyright 2014 李松泉 功能说明：权限拦截器，判断用户是否有调用的权限 开发时间：2014-1-10上午11:16:30 开发作者：李松泉
 */
public class AuthInterceptor implements Interceptor {

	public void intercept(ActionInvocation ai) {
		Controller controller = ai.getController();
		User loginUser = controller.getSessionAttr("loginUser");
		if (loginUser == null)
			controller.redirect("/common/login.jsp");
		else {
			int userId = loginUser.getInt("id");
			if (Resources.isNeedAuthControl(ai.getActionKey())) {// 判断资源是否被配置，未配置则不受控制
				if (loginUser.canVisit(userId, ai.getControllerKey(),
						ai.getActionKey()))
					ai.invoke();
				else {
					controller.render("/error/no-authority.jsp");
				}
			} else
				ai.invoke();
		}

	}
}
