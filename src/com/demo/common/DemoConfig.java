package com.demo.common;

import com.demo.blog.Blog;
import com.demo.blog.BlogController;
import com.demo.user.Resources;
import com.demo.user.ResourcesController;
import com.demo.user.Role;
import com.demo.user.RoleController;
import com.demo.user.User;
import com.demo.user.UserController;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.plugin.shiro.ShiroInterceptor;
import com.jfinal.ext.plugin.shiro.ShiroPlugin;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;

/**
 * API引导式配置
 */
public class DemoConfig extends JFinalConfig {

	Routes routes;

	/**
	 * 配置常量
	 */
	@Override
	public void configConstant(Constants me) {
		loadPropertyFile("a_little_config.txt"); // 加载少量必要配置，随后可用getProperty(...)获取值
		me.setDevMode(getPropertyToBoolean("devMode", false));
		me.setViewType(ViewType.JSP); // 设置视图类型为Jsp，否则默认为FreeMarker
		me.setErrorView(401, "/common/401.jsp");
		me.setErrorView(403, "/common/403.jsp");
		me.setError404View("/common/404.jsp");
		me.setError500View("/common/500.jsp");

	}

	/**
	 * 配置路由
	 */
	@Override
	public void configRoute(Routes me) {
		this.routes = me;
		me.add("/", CommonController.class);
		me.add("/common", CommonController.class);
		me.add("/blog", BlogController.class);
		me.add("/user", UserController.class);
		me.add("/resources", ResourcesController.class);
		me.add("role", RoleController.class);
	}

	/**
	 * 配置插件
	 */
	@Override
	public void configPlugin(Plugins me) {
		// 配置C3p0数据库连接池插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"),
				getProperty("user"), getProperty("password").trim());
		me.add(c3p0Plugin);

		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		me.add(arp);
		arp.addMapping("jf_demo_blog", Blog.class); // 映射blog 表到 Blog模型
		arp.addMapping("jf_demo_user", User.class);
		arp.addMapping("jf_demo_role", Role.class);
		arp.addMapping("jf_demo_resources", Resources.class);

		me.add(new ShiroPlugin(routes));
		// 缓存插件
		me.add(new EhCachePlugin());
	}

	/**
	 * 配置全局拦截器
	 */
	@Override
	public void configInterceptor(Interceptors me) {
//		 me.add(new AuthInterceptor());
		me.add(new ShiroInterceptor());
	}

	/**
	 * 配置处理器
	 */
	@Override
	public void configHandler(Handlers me) {

	}

	/**
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		JFinal.start("WebRoot", 8080, "/", 5);
	}
}
