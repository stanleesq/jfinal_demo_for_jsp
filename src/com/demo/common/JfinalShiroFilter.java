package com.demo.common;

import java.util.Map;

import org.apache.shiro.web.filter.mgt.FilterChainManager;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.AbstractShiroFilter;

public class JfinalShiroFilter extends AbstractShiroFilter {

	@Override
	public void init() throws Exception {
		DefaultWebSecurityManager dwsm = new DefaultWebSecurityManager(
				new ShiroDbRealm());
		setSecurityManager(dwsm);

		JfinalShiroFilterFactory factory = new JfinalShiroFilterFactory();
		factory.setSuccessUrl("/blog");
		factory.setLoginUrl(filterConfig.getInitParameter("loginUrl"));
		Map<String, String> filterChainDefinitionMap = factory.getFilterChainDefinitionMap();
		// filterChainDefinitionMap.put(filterConfig.getInitParameter("authc"),
		// "authc");
		// String unauthorizedUrls =
		// filterConfig.getInitParameter("unauthorizedUrls");
		// for(String unauthorizedurl : unauthorizedUrls.split(",")){
		// filterChainDefinitionMap.put(unauthorizedurl, "anon");
		// }
		// filterChainDefinitionMap.put("/common/**", "anon");
		// filterChainDefinitionMap.put("/dwz/**", "anon");
		// filterChainDefinitionMap.put("/users/**", "roles[admin]");
		// filterChainDefinitionMap.put("/**", "user");
		factory.setFilterChainDefinitionMap(filterChainDefinitionMap);

		FilterChainManager manager = factory.createFilterChainManager();
		PathMatchingFilterChainResolver chainResolver = new PathMatchingFilterChainResolver();
		chainResolver.setFilterChainManager(manager);

		setFilterChainResolver(chainResolver);
	}

}
