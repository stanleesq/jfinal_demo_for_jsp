package com.demo.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

import com.demo.user.Resources;
import com.demo.user.Role;
import com.demo.user.User;

/**
 * Copyright 2014 李松泉 功能说明：TODO 开发时间：2014-4-17下午1:18:07 开发作者：李松泉
 */
public class ShiroDbRealm extends AuthorizingRealm {

	/**
	 * 认证回调函数,登录时调用.
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken authcToken) throws AuthenticationException {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		String userName = token.getUsername().toString();
		String password = String.valueOf(token.getPassword());
		User user = User.getUser(userName, password);
		if (user != null) {
			int userid = user.getInt("id");
			String username = user.getStr("name");
			String useremail = user.getStr("email");
			String userpassword = user.getStr("password");
			return new SimpleAuthenticationInfo(new ShiroUser(userid, username,
					useremail), userpassword, username);
		} else {
			return null;
		}
	}

	/**
	 * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(
			PrincipalCollection principals) {
		ShiroUser shiroUser = (ShiroUser) principals.getPrimaryPrincipal();
		int userId = shiroUser.id;
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		List<Role> userRoleList = Role.getRoleList(userId);
		List<String> resourceStrList = new ArrayList<String>();
		for (Role roleTemp : userRoleList) {
			info.addRole(roleTemp.getStr("name"));
			int roleId = roleTemp.getInt("id");
			List<Resources> resourcesList = Resources.getResourcesList(roleId);
			for (Resources resTemp : resourcesList) {
				resourceStrList.add(resTemp.getStr("action_key"));
				Resources.userActionKeyList.add(resTemp.getStr("action_key"));
			}
		}
		info.addStringPermissions(resourceStrList);
		return info;
	}

	/**
	 * 更新用户授权信息缓存.
	 */
	public void clearCachedAuthorizationInfo(Object principal) {
		SimplePrincipalCollection principals = new SimplePrincipalCollection(
				principal, getName());
		clearCachedAuthorizationInfo(principals);
	}

	/**
	 * 清除所有用户授权信息缓存.
	 */
	public void clearAllCachedAuthorizationInfo() {
		Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
		if (cache != null) {
			for (Object key : cache.keys()) {
				cache.remove(key);
			}
		}
	}

	/**
	 * 自定义Authentication对象，使得Subject除了携带用户的登录名外还可以携带更多信息.
	 */
	public static class ShiroUser implements Serializable {
		private static final long serialVersionUID = -1373760761780840081L;
		public int id;
		public String loginName;
		public String name;

		public ShiroUser(int id, String loginName, String name) {
			this.id = id;
			this.loginName = loginName;
			this.name = name;
		}

		public String getName() {
			return name;
		}

		/**
		 * 本函数输出将作为默认的<shiro:principal/>输出.
		 */
		@Override
		public String toString() {
			return loginName;
		}

		/**
		 * 重载equals,只计算loginName;
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			ShiroUser other = (ShiroUser) obj;
			if (loginName == null) {
				if (other.loginName != null) {
					return false;
				}
			} else if (!loginName.equals(other.loginName)) {
				return false;
			}
			return true;
		}
	}

	/**
	 * 得到当前登录用户
	 * 
	 * @return
	 */
	public static ShiroUser getLoginUser() {
		return (ShiroUser) SecurityUtils.getSubject().getPrincipal();
	}
}
