package com.demo.user;

import com.jfinal.core.Controller;

/**
 * Copyright 2014 李松泉 
 * 功能说明：资源管理Controller 
 * 开发时间：2014-1-10下午5:24:03
 * 开发作者：李松泉
 */
public class ResourcesController extends Controller{
	
	public void index(){
		setAttr("resourcesList", Resources.getAllResources());
		render("/user/resources.jsp");
	}
	
	public void edit(){
		if(getParaToInt(0) != null){
			int resourcesId = getParaToInt(0);
			Resources resources = Resources.dao.findById(resourcesId);
			setAttr("resources", resources);
		}
		render("/user/resources-input.jsp");
	}
	
	public void save(){
		if(getPara("resources.id") != null && !"".equals(getPara("resources.id")))
			getModel(Resources.class).update();
		else
			getModel(Resources.class).save();
		redirect("/resources");
	}
	
	public void delete(){
		Resources.dao.deleteById(getParaToInt());
		redirect("/resources");
	}
}
