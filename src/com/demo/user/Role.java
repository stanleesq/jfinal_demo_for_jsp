package com.demo.user;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

/**
 * Copyright 2014 李松泉 
 * 功能说明：TODO 
 * 开发时间：2014-1-10上午9:19:30
 * 开发作者：李松泉
 */
@SuppressWarnings("serial")
public class Role extends Model<Role>{
	public static final Role dao = new Role();

	public static Object getAllRole() {
		return dao.find("select * from jf_demo_role order by id asc");
	}

	public static List<Role> getRoleList(int userId) {
		String sql = "select t1.id,t1.name from jf_demo_role t1 where t1.id in ("+"select t2.role_id from jf_demo_user_role t2 where t2.user_id = '"+userId+"'"+")";
		return dao.find(sql);
	}
}
