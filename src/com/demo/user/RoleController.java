package com.demo.user;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.core.Controller;

/**
 * Copyright 2014 李松泉 
 * 功能说明：TODO 
 * 开发时间：2014-1-13下午1:22:04
 * 开发作者：李松泉
 */
public class RoleController extends Controller{
	
	public void index(){
		setAttr("roleList", Role.getAllRole());
		render("/user/role.jsp");
	}
	
	public void edit(){
		if(getParaToInt(0) != null){
			int roleId = getParaToInt(0);
			Role role = Role.dao.findById(roleId);
			setAttr("role", role);
			List<Resources> checkedResList = Resources.getCheckedResources(roleId);
			List<Integer> checkedResIdList = new ArrayList<Integer>();
			for(Resources res : checkedResList){
				checkedResIdList.add(res.getInt("resources_id"));
			}
			setAttr("checkedResIdList", checkedResIdList);
		}
		setAttr("allResList", Resources.getAllResources());
		render("/user/role-input.jsp");
	}
	
	public void save(){
		if(getPara("role.id") != null && !"".equals(getPara("role.id")))
			getModel(Role.class).update();
		else
			getModel(Role.class).save();
		redirect("/role");
	}
	
	public void delete(){
		Role.dao.deleteById(getParaToInt(0));
		redirect("/role");
	}
}
