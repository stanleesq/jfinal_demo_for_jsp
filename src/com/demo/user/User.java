package com.demo.user;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Model;

/**
 * Copyright 2014 李松泉 
 * 功能说明：TODO 
 * 开发时间：2014-1-10上午9:12:54
 * 开发作者：李松泉
 */
@SuppressWarnings("serial")
public class User extends Model<User>{
	
	public static final User dao = new User();
	
	/**
	 * 判断用户是否有调用该方法的权限，true:有权限; false:无权限
	 * @param userId 用户id
	 * @param controllerKey Controller名称
	 * @param actionKey Action(方法)名称
	 * @return
	 */
	public boolean canVisit(int userId, String controllerKey, String actionKey) {
		Map<String,List<String>> thisAuthMap = new HashMap<String, List<String>>();
		if(Resources.antuMap != null && !Resources.antuMap.isEmpty())
			thisAuthMap = Resources.antuMap;
		else
			thisAuthMap = Resources.getAuthority(userId);
		List<String> actionKeyList = thisAuthMap.get(controllerKey);
		if(actionKeyList != null && actionKeyList.contains(actionKey))
			return true;
		else
			return false;
	}

	public static User getUser(String loginUserName, String loginPassword) {
		String sql = "select t.id, t.name, t.password, t.email from jf_demo_user t where t.name = '"+loginUserName+"' and t.password = '"+loginPassword+"'";
		return dao.findFirst(sql);
	}

	public static Object getAllUser() {
		return dao.find("select * from jf_demo_user order by id asc");
	}

	public static User getUser(String userName) {
		String sql = "select t.id, t.name, t.password from jf_demo_user t where t.name = '"+userName+"'";
		return dao.findFirst(sql);
	}
}
