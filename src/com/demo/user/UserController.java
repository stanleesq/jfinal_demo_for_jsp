package com.demo.user;

import com.jfinal.core.Controller;

/**
 * Copyright 2014 李松泉 
 * 功能说明：TODO 
 * 开发时间：2014-1-10下午2:51:38
 * 开发作者：李松泉
 */
public class UserController extends Controller{
	
	public void index() {
		setAttr("userList", User.getAllUser());
		render("user.jsp");
	}
	
}
