package com.demo.user;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

/**
 * Copyright 2014 李松泉 
 * 功能说明：TODO 
 * 开发时间：2014-1-10下午3:10:41
 * 开发作者：李松泉
 */
public class UserValidator extends Validator{

	@Override
	protected void validate(Controller c) {
		validateRequired("loginUserName", "loginUserNameMsg", "请输入用户名");
		validateRequired("loginPassword", "loginPasswordMsg", "请输入密码");
	}

	@Override
	protected void handleError(Controller c) {
		c.keepModel(User.class);
		c.render("/common/login.jsp");
	}
}
